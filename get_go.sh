#!/bin/bash

#
# this small script is a convenience to grab the go compiler stack
# set up the proper environment variables to compile the project
#

if [ "$(which wget)" = "" ]; then
  echo "Installing wget..."
  sudo apt-get install wget
fi

wget https://storage.googleapis.com/golang/go1.4.2.linux-amd64.tar.gz

tar xvzf go1.4.2.linux-amd64.tar.gz && rm go1.4.2.linux-amd64.tar.gz && cd ./go/src

echo "Compiling necessary files..."

./make.bash

echo "Setting environment variables, please add these to your .bashrc for future use"

cd ../../

mkdir godev && export GOPATH=$(pwd)/godev
mkdir godev/src && mkdir godev/bin && mkdir godev/pkg

export GOBIN=$(pwd)/go/bin

mkdir $GOPATH/src/binit && cp -Ra ./crypto ./*.go ./bin $GOPATH/src/binit/

cd $GOPATH/src/binit

echo "Go is installed and ready to compile"

$GOBIN/go build

$GOBIN/go install

ln -s $(pwd)/binit ../../../binit
