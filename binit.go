// binit project main.go
package main

import (
	bin "binit/bin"
	"binit/crypto"
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func ReadStdin() ([]byte, error) {
	return ioutil.ReadAll(os.Stdin)
}

func ReadPW(verify bool) string {
	rdr := bufio.NewReader(os.Stdin)

start_over:

	fmt.Printf(" Enter Password: ")
	pw, _ := rdr.ReadString('\n')

	if len(pw) != 0 {
		pw = pw[:len(pw)-1]
	} else {
		return ""
	}

	if verify == true {
		fmt.Printf("Retype Password: ")
		pw2, _ := rdr.ReadString('\n')

		if len(pw2) != 0 {
			pw2 = pw2[:len(pw2)-1]
		} else {
			return ""
		}

		if pw2 != pw {
			fmt.Printf("Password Don't Match!")
			goto start_over
		}
	}

	return pw
}

func main() {

	var inputBuffer []byte
	var placeholder []byte
	var err error
	//var cryptoFunc []byte
	//var hashFunc []byte
	var pw []byte

	for i := 0; i < len(os.Args); i++ {

		arg := os.Args[i]

		switch {
		case arg == "--": // Potentially long STDIN flag
			inputBuffer, err = ReadStdin()
			if err != nil {
				panic(err) // TODO Handle errors
			}
		case arg == "-": // potentially a mistype of STDIN flag
			i++
			if i >= len(os.Args) || os.Args[i] != "-" {
				panic("found - flag but no indicator of source")
			}

			inputBuffer, err = ReadStdin()
			if err != nil {
				panic(err) // TODO Handle errors
			}
		case arg == "-m": // a short ASCII message
			i++
			if i >= len(os.Args) {
				panic("-m flag used but no message provided")
			}

			inputBuffer = []byte(os.Args[i])
		case arg == "-p":
			i++
			if i >= len(os.Args) {
				panic("-m flag used but no message provided")
			}

			pw = []byte(os.Args[i])
		case arg == "-d": // download a placeholder
			i++
			if i >= len(os.Args) {
				panic("-d flag used but no placeholder provided")
			}

			placeholder = []byte(os.Args[i])
		case arg == "-c": // select the crypto algorithm (only effective for encrypt)
			i++
			if i >= len(os.Args) {
				panic("-c flag used but no crypto function selected. use '-c ?' for a listing of available functions.")
			}

			if os.Args[i] == "?" {
				crypto.PrintOps()
			} else {

			}
		case arg == "-h":
			i++
			if i >= len(os.Args) {
				panic("-h flag used but no hash function selected. use '-h ?' for a listing of available functions.")
			}

			if os.Args[i] == "?" {
				crypto.PrintOps()
			} else {

			}
		}
	}

	os.Stdin.Truncate(0)

	var Quiet = false

	if placeholder != nil {

		Quiet = true

		if pw == nil {
			pw = []byte(ReadPW(false))
		}

		if len(pw) == 0 {
			panic("no password specified, and non-interactive mode selected")
		}

		//		fmt.Printf("DEBUG: Placeholder: %s", placeholder)
		//		fmt.Printf("PW: '%s'", pw)

		page, err := bin.FindPublicGist(string(placeholder), 10)
		if err != nil {
			panic(err)
		}

		new_cb, err := bin.DecodeGist(page)
		if err != nil {
			panic(err)
		}

		plaintext, err := bin.DecryptBin(new_cb, crypto.Password2Key(string(pw)))
		if err != nil {
			panic(err)
		}

		fmt.Print(string(plaintext))
	}

	if inputBuffer != nil {

		title := bin.BuildTitle(bin.GetWords(2), 100)

		c := crypto.CFBCrypt{}
		h := crypto.SHA256Hash{}

		if pw == nil {
			pw = []byte(ReadPW(true))
		}

		if len(pw) == 0 {
			panic("no password specified, and non-interactive mode selected")
		}

		//		fmt.Printf("PW: '%s'", pw)

		//		fmt.Printf("DEBUG: InputBuffer: %s", string(inputBuffer))

		cb, err := bin.EncryptBin(inputBuffer, crypto.Password2Key(string(pw)), c, h)
		if err != nil {
			panic(err)
		}

		cb = cb

		urls, _ := bin.CreateGist(cb, title)

		if Quiet == true {
			os.Stderr.WriteString("Stored Document to Placeholder : " + title + "\n")
			os.Stderr.WriteString(urls["html_url"].(string) + "\n")
		} else {
			fmt.Println("Stored Document to Placeholder : ", title)
			fmt.Println(urls["html_url"])
		}
	}
}
