# Quick Start
A command line tool to post AES encrypted messages to Github Gists and retrieve them again quickly and easily by any user with knowledge of the passphrase.

It really is as simple as

```bash
  $ binit -m 'Some secret API code only I can know about : BAF321F-!f2F#$F'
  Choose a Password:
  Stored Document to Placeholder :  SofiaAdam57
  https://gist.github.com/fdec8b1a28911efd70f4
```

And from any other machine, with no prior attachments (save a proximity in time, the longer between posting and retrieving, the more requests we have to make
to Github to search for it!).  Within a couple hours you'll be fine though, otherwise login with your Github account for maximum persistance. (still to be implemented)

```bash
  $ binit -d 'SofiaAdam57'
  Enter the Password:
  Some secret API code only I can know about : BAF321F-!f2F#$F
```

# More Details

To encrypt a file or archive, use the stdin piping ability:

```bash
   $ tar czv crypto/ | ./binit -- -p 'mysource'
      BinIT Crypto Module
      crypto/
      crypto/hmac_test.go
      crypto/crypto.go
      crypto/cbc_test.go
      crypto/hmac.go
      crypto/ctr.go
      crypto/ofb_test.go
      crypto/ofb.go
      crypto/cbc.go
      crypto/sha256.go
      crypto/cfb_test.go
      crypto/encrypt.go
      crypto/cfb.go
      crypto/sha256_test.go
      crypto/ctr_test.go
      Stored Document to Placeholder :  ResultCrystal92
      https://gist.github.com/30b7f56f51569caf7a29
 ```

 To choose a different cipher (the default is AES with a chained block cipher):

 ```bash
  $ binit -c ?
    Ciphers Available:
      aes_cbc_192     aes_cfb_192     aes_ctr_192     aes_ofb_192
    Hash Functions Available:
      sha256          hmac
  $ binit -c aes_ctr_192 -h hmac -m "Mmmm.. Streaming encryption!"
    ...
  ```


# Description of Command Line Flags

	-m 	"A Short Message"	A convenience flag for uploading a short message

	--						Indicates to read input from Standard Input (STDIN)
							Input can include non-printable characters or other
							binary data, it will be recreated upon decryption
							exactly byte-for-byte as input.

							*Limitations* : Large files are not well handled
											( > 1 MB ) as of yet, including
											Github's special handling of files
											greater than this size.

								* See the -p flag below *

	-d 	PlaceHolder01		Searches for and downloads the indicated posting,
							this method is CASE-SENSITIVE so make sure you
							capitalize the two words as displayed.

	-c [?]|aes_..._192		Selects a different cipher algorithm, ? will display
							the available cipher and hash algorithms and
							terminate.

	-p <password>			When using STDIN as input the tool enters non-
							interactive mode and it becomes impossible to enter
							a password after execution has begun.  Therefore,
							you must choose a password on the command line.

							Be aware your password will be visible in your bash
							history as well as in the process listing.

	-h [?]|sha256|hmac		Selects the internal data verification hash, ? will
							display the current available options. HMAC will
							always be used on the message as a whole, to ensure
							overall integrity, however, the internal hash (pre-
							encryption) is what's being selected.

# The Makefile

Available commands include commands to download the go library in the same
folder, as well as commands to benchmark, test and build the tool itself:

	make build				will download the Go SDK if not available in the
							the system path, set up environment variables and
							compile the 'binit' tool.

	make test				will run the internal tests to verify integrity of
							various components.  The tests can be modified or
							viewed in every file ending in a _test.go extension.

	make benchmark			runs internal benchmarking of several methods with
							automatic (Golang provided) adjustments for number
							of rounds of execution.


# Licensing
The MIT License (MIT)

Copyright (c) 2015 Chris Pergrossi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
