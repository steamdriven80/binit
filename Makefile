# Makefile
# Author: Chris Pergrossi
#
# This file is a quick shortcut to those who want to compile
# the code themselves.
#
# Prerequisites:
#		  - Golang Compiler and a properly set up GOPATH
#			with this git repo located under $GOPATH/src/...
#

build: ./**/*.go
	if [ "`which go`" = "" ]; then ./get_go.sh; else go build; fi

test: ./**/*.go
	cd crypto && go test
	cd bin && go test

benchmark: ./**/*.go
	cd crypto && go test --bench .
	cd bin && go test --bench .
