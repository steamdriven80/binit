package binit

import (
	"binit/crypto"
	"errors"
)

func init() {

}

type PlainBin struct {
	Hash      []byte
	Plaintext []byte
}

type CryptBin struct {
	Ciphertext []byte
	HMAC       []byte
	c          crypto.CryptoSet
	h          crypto.HashSet
}

var (
	APP_NAME = "binit"
	VERSION  = "0.8"
	AUTHOR   = "Chris Pergrossi"
	HOMEPAGE = "https://github.com/gearmover/binit"
)

func EncryptBin(msg []byte, key []byte, c crypto.CryptoSet, h crypto.HashSet) (*CryptBin, error) {

	pb := PlainBin{Plaintext: msg}

	ho := crypto.SHA256Hash{}
	var err error
	pb.Hash, err = ho.Hash(pb.Plaintext, key)
	if err != nil {
		return nil, err
	}

	cb := CryptBin{}

	cb.Ciphertext, err = c.Encrypt(string(pb.Hash)+string(pb.Plaintext), key)
	if err != nil {
		return nil, err
	}

	cb.HMAC, err = h.Hash(cb.Ciphertext, key)
	if err != nil {
		return nil, err
	}

	cb.c = c
	cb.h = h

	return &cb, nil
}

var ErrInvalidMAC = errors.New("[binit::DecryptBin] -> invalid MAC verification value. message is corrupt.")

func DecryptBin(cb *CryptBin, key []byte) (message []byte, err error) {

	checkmac, err := cb.h.Hash(cb.Ciphertext, key)
	if err != nil {
		return nil, err
	}

	if err := cb.h.Verify(checkmac, cb.HMAC); err != nil {
		return nil, ErrInvalidMAC
	}

	plaintext, err := cb.c.Decrypt(cb.Ciphertext, key)
	if err != nil {
		return nil, err
	}

	SHA := plaintext[:32]
	plaintext = plaintext[32:]

	HashFunc := crypto.SHA256Hash{}

	checkhash, err := HashFunc.Hash([]byte(plaintext), key)
	if err != nil {
		return nil, err
	}

	if HashFunc.Verify(checkhash, []byte(SHA)) != nil {
		return nil, ErrInvalidMAC
	}

	return []byte(plaintext), nil
}
