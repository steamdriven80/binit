package binit

import (
	bincrypt "binit/crypto"
	"bufio"
	"bytes"
	"encoding/hex"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var (
	GithubAPI            = "https://api.github.com"
	GithubCreateGist     = "/gists"              // post
	GithubListPublic     = "/gists/public"       // get
	GithubListPublicPage = "/gists/public?page=" // get
	GithubListGists      = "/gists"              // get
)

type CreateGistRqst map[string]interface{}

func CreateGist(cb *CryptBin, name string) (response map[string]interface{}, err error) {

	rqst := make(CreateGistRqst)

	content := make(map[string]string)

	content["data"] = hex.EncodeToString(cb.Ciphertext)
	content["hmac"] = hex.EncodeToString(cb.HMAC)
	c, _ := cb.c.Implements()
	content["ctype"] = string(c[0])

	h, _ := cb.h.Implements()
	content["htype"] = string(h[0])

	rqst["description"] = "Encrypted Data [ " + name + " ] - created using " + APP_NAME + " v" + VERSION + " <" + HOMEPAGE + ">"
	rqst["public"] = true
	rqst["files"] = make(map[string]interface{})
	rqst["files"].(map[string]interface{})[name+".json"] = make(map[string]string)

	bslice, err := json.Marshal(content)

	var fslice string

	if len(bslice) > 80 {
		var lines = int(len(bslice)/80.0) + 1

		var start, stop int

		stop = 80
		for i := 0; i < lines; i++ {

			if stop > len(bslice) {
				stop = len(bslice)
			}
			if start > stop {
				start = 0
			}

			fslice += string(bslice[start:stop])
			fslice += "\n"

			start = stop
			stop += 80
		}
	} else {
		fslice = string(bslice)
	}

	rqst["files"].(map[string]interface{})[name+".json"].(map[string]string)["content"] = fslice

	if err != nil {
		panic(err)
	}

	msg, err := json.Marshal(rqst)
	if err != nil {
		panic(err)
	}

	buf := bytes.NewBuffer(msg)
	resp, err := http.Post(GithubAPI+GithubCreateGist, "application/json", buf)
	if err != nil {
		panic(err)
	}

	body := bufio.NewReader(resp.Body)

	var URLs map[string]interface{}

	respData, _ := body.ReadBytes('\000')

	err = json.Unmarshal(respData, &URLs)
	if err != nil {
		panic(err)
	}

	resp.Body.Close()

	return URLs, nil
}

func FindPublicGist(name string, MaxPage int) (document []byte, err error) {
	resp, err := http.Get(GithubAPI + GithubListPublic)
	if err != nil {
		panic(err)
	}

	rdr := bufio.NewReader(resp.Body)
	obj, _ := rdr.ReadString('\000')

	resp.Body.Close()

	if index := strings.Index(obj, name); index != -1 {
		goto conclusion
	}

	for page := 1; page < MaxPage; page++ {

		resp, err = http.Get(GithubAPI + GithubListPublicPage + strconv.Itoa(page+1))
		if err != nil {
			panic(err)
		}

		rdr = bufio.NewReader(resp.Body)
		obj, _ = rdr.ReadString('\000')

		resp.Body.Close()

		if index := strings.Index(obj, name); index != -1 {
			goto conclusion
		}
	}

	return nil, errors.New("Unable to Find Requested Document")

conclusion:

	index := strings.Index(obj, name)

	contentIndex := strings.Index(obj[index:], `"raw_url":"`)

	if contentIndex == -1 {
		panic("Couldn't find the raw url!")
	}

	contentIndex += len(`"raw_url":"`) + index

	endIndex := strings.Index(obj[contentIndex:], "\"")

	if endIndex == -1 {
		panic("Couldn't find a closing quotation!")
	}

	endIndex += contentIndex

	raw_url := obj[contentIndex:endIndex]

	resp, _ = http.Get(raw_url)

	rdr = bufio.NewReader(resp.Body)
	doc, _ := rdr.ReadBytes('\000')

	resp.Body.Close()

	doc = []byte(strings.Replace(string(doc), "\n", "", -1))

	return doc, nil
}

type GistContent struct {
	Data      string `json:data`
	HMAC      string `json:hmac`
	HashType  string `json:htype`
	CryptType string `json:ctype`
}

func DecodeGist(document []byte) (cb *CryptBin, err error) {

	/*
		content := make(map[string]string)

		content["data"] = hex.EncodeToString(cb.Ciphertext)
		content["hmac"] = hex.EncodeToString(cb.HMAC)
		c, _ := cb.c.Implements()
		content["c"] = string(c[0])

		h, _ := cb.h.Implements()
		content["h"] = string(h[0])

		rqst["description"] = "encrypted temporary data"
		rqst["public"] = true
		rqst["files"] = make(map[string]interface{})
		rqst["files"].(map[string]interface{})[name+".json"] = make(map[string]string)
	*/

	var rqst GistContent

	err = json.Unmarshal(document, &rqst)
	if err != nil {
		log.Fatal(err)
	}

	cb = &CryptBin{}

	cb.HMAC, _ = hex.DecodeString(rqst.HMAC)
	cb.Ciphertext, _ = hex.DecodeString(rqst.Data)

	start := strings.Index(string(document), `"htype":"`) + len(`"htype":"`)
	stop := strings.Index(string(document[start:]), `"`)
	htype := string(document[start : start+stop])

	start = strings.Index(string(document), `"ctype":"`) + len(`"ctype":"`)
	stop = strings.Index(string(document[start:]), `"`)
	ctype := string(document[start : start+stop])

	switch {
	case ctype == "aes-cbc-192":
		cb.c = bincrypt.CBCCrypt{}
	case ctype == "aes-cfb-192":
		cb.c = bincrypt.CFBCrypt{}
	case ctype == "aes-ctr-192":
		cb.c = bincrypt.CTRCrypt{}
	case ctype == "aes-ofb-192":
		cb.c = bincrypt.OFBCrypt{}
	default:
		log.Printf("UNKNOWN CRYPT TYPE: %s", rqst.CryptType)
	}

	switch {
	case htype == "hmac":
		cb.h = bincrypt.HMACHash{}
	case htype == "sha256":
		cb.h = bincrypt.SHA256Hash{}
	default:
		log.Printf("UNKNOWN HASH: %s", rqst.HashType)
	}

	return cb, nil
}
