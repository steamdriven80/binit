package crypto

import (
	"crypto/hmac"
	"crypto/sha256"
)

/*
The Golang native HMAC hashing function is well implemented and we simply provide
a wrapper to integrate it with our own application.

From the Golang documentation:
Package hmac implements the Keyed-Hash Message Authentication Code (HMAC) as
defined in U.S. Federal Information Processing Standards Publication 198. An
HMAC is a cryptographic hash that uses a key to sign a message. The receiver
verifies the hash by recomputing it using the same key.
*/
type HMACHash struct{}

func (h HMACHash) Hash(msg bitstring, key bitstring) (hash bitstring, err error) {

	mac := hmac.New(sha256.New, key)
	if _, err := mac.Write(msg); err != nil {
		panic(err)
	}

	return mac.Sum(nil), nil
}

/*
We use a special comparison function here that operates in constant time, in order
to prevent certain types of attacks on the hashing function. Returns nil on equality
and an InvalidArgs error if not.
*/
func (h HMACHash) Verify(hashA bitstring, hashB bitstring) (err error) {

	if hmac.Equal(hashA, hashB) == true {
		return nil
	}

	return ErrInvalidArgs
}

/*

*/
func (c HMACHash) Describe() (howtouse string, err error) {
	return "", ErrNotImplemented
}

/*

*/
func (c HMACHash) Implements() (cops []string, err error) {
	ops := []string{
		"hmac",
	}

	return ops, nil
}
