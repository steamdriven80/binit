package crypto

import (
	"encoding/hex"
	"testing"
)

func TestCFBCrypt(t *testing.T) {

	cfb := CFBCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	ciphertext, err := cfb.Encrypt(message, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	plaintext, err := cfb.Decrypt(ciphertext, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	if plaintext != message {
		t.Error("Decrypted Msg != Original Msg")
	}
}

func BenchmarkCFBEncrypt(b *testing.B) {
	cfb := CFBCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		cfb.Encrypt(message, []byte(key))
	}
}

func BenchmarkCFBDecrypt(b *testing.B) {
	cfb := CFBCrypt{}

	message, _ := hex.DecodeString("d938612b08769ac7cfd034b8477a45c92abca54962c4fa23072724d89f3b25491a2b2b2f0ab76103d454b03584db93b0")
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		cfb.Decrypt(message, []byte(key))
	}
}
