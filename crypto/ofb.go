package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"log"
)

type OFBCrypt struct{}

func (c OFBCrypt) Encrypt(plaintext string, key bitstring) (ciphertext bitstring, err error) {
	// here we allocate a new aes block stream
	block, err := aes.NewCipher(key)
	if err != nil {
		log.Printf(err.Error())
		return bitstring(""), err
	}

	// generate an IV set
	iv, _ := generateIV(AESBlockSize)

	// here we allocate the memory for our ciphertext, and
	// prepend the IV values to it.  We don't need the IV to be
	// secret, just unique.
	ciphertext = make(bitstring, AESBlockSize+len(plaintext))
	copy(ciphertext[:AESBlockSize], iv)

	// here we allocate a scratch pad to let us move through the plaintext byte by byte
	stream := cipher.NewOFB(block, iv)
	stream.XORKeyStream(ciphertext[AESBlockSize:], bitstring(plaintext))

	return ciphertext, nil
}

func (c OFBCrypt) Decrypt(ciphertext bitstring, key bitstring) (plaintext string, err error) {
	// here we allocate a new aes block stream
	block, err := aes.NewCipher(key)
	if err != nil {
		log.Printf(err.Error())
		return "", err
	}

	// SANITY CHECK
	if len(ciphertext) < AESBlockSize {
		//log.Printf("[Error] -> ciphertext is too short to be valid.  message has been altered.")
		//return "", ErrInvalidArgs
	}

	// we store the IV at the beginning of the ciphertext when encrypting
	iv := ciphertext[:AESBlockSize]
	ciphertext = ciphertext[AESBlockSize:]

	// OFB mode is the same for both encryption and decryption, so we can
	// decrypt the ciphertext the same way we encrypted it
	pt := make([]byte, len(ciphertext))
	stream := cipher.NewOFB(block, iv)
	stream.XORKeyStream(pt, ciphertext)

	return string(pt), nil
}

func (c OFBCrypt) Describe() (howtouse string, err error) {
	return "", ErrNotImplemented
}

func (c OFBCrypt) Implements() (cops []string, err error) {
	ops := []string{
		"aes-ofb-192",
	}

	return ops, nil
}
