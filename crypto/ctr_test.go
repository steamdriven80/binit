package crypto

import (
	"encoding/hex"
	"testing"
)

func TestCTRCrypt(t *testing.T) {

	ctr := CTRCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	ciphertext, err := ctr.Encrypt(message, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	plaintext, err := ctr.Decrypt(ciphertext, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	if plaintext != message {
		t.Error("Decrypted Msg != Original Msg")
	}
}

func BenchmarkCTREncrypt(b *testing.B) {
	ctr := CTRCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		ctr.Encrypt(message, []byte(key))
	}
}

func BenchmarkCTRDecrypt(b *testing.B) {
	ctr := CTRCrypt{}

	message, _ := hex.DecodeString("1c4d867047bb7f19fe398a3f4e140b101d7e0cf7a99460a1b52c95ceb5200677f9c55c57b0f0")
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		ctr.Decrypt(message, []byte(key))
	}
}
