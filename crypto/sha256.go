package crypto

import (
	"crypto/sha256"
)

/*
The Golang native HMAC hashing function is well implemented and we simply provide
a wrapper to integrate it with our own application.

From the Golang documentation:
Package hmac implements the Keyed-Hash Message Authentication Code (HMAC) as
defined in U.S. Federal Information Processing Standards Publication 198. An
HMAC is a cryptographic hash that uses a key to sign a message. The receiver
verifies the hash by recomputing it using the same key.
*/
type SHA256Hash struct{}

func (o SHA256Hash) Hash(msg bitstring, key bitstring) (hash bitstring, err error) {

	h := sha256.New()
	if _, err := h.Write(msg); err != nil {
		panic(err)
	}

	return h.Sum(nil), nil
}

/*
We use a special comparison function here that operates in constant time, in order
to prevent certain types of attacks on the hashing function. Returns nil on equality
and an InvalidArgs error if not.
*/
func (o SHA256Hash) Verify(hashA bitstring, hashB bitstring) (err error) {

	if len(hashA) != len(hashB) {
		return ErrInvalidArgs
	}

	for i, x := range hashA {
		if x != hashB[i] {
			return ErrInvalidArgs
		}
	}

	return nil
}

/*

*/
func (c SHA256Hash) Describe() (howtouse string, err error) {
	return "", ErrNotImplemented
}

/*

*/
func (c SHA256Hash) Implements() (cops []string, err error) {
	ops := []string{
		"sha256",
	}

	return ops, nil
}
