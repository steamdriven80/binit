package crypto

import (
	"testing"
)

func TestHMACHash(t *testing.T) {

	h := HMACHash{}

	msg := "Super Sekrets Subscription"
	const key = "0123456789-%ABS@"

	mac, _ := h.Hash([]byte(msg), []byte(key))

	// mess up the message, make sure it detects it

	msg = "K" + msg[1:]

	checkmac, _ := h.Hash([]byte(msg), []byte(key))
	if h.Verify(mac, []byte(checkmac)) == nil {
		t.Fail()
	}

	// fix the message, make sure it verifies

	msg = "S" + msg[1:]

	checkmac, _ = h.Hash([]byte(msg), []byte(key))
	if h.Verify(mac, []byte(checkmac)) != nil {
		t.Fail()
	}
}
