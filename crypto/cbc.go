package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"log"
)

type CBCCrypt struct{}

/*
Wraps the native Golang Cipher Block Chain cryptographic algorithm.  From
the GoDocs :

""
CBC provides confidentiality by xoring (chaining) each plaintext block
with the previous ciphertext block before applying the block cipher.

See NIST SP 800-38A, pp 10-11
""

Encrypt takes a plaintext value of arbitrary length, which is expected to already
include a MAC verification value if necessary, and a key that is the correct length
(AESBlockSize = 16 bytes) and returns a ciphertext in byte string format (i.e. not
all printable characters!  Use hex.EncodeToString() or a base64 conversion before
printing.

The IV values are generated from Golangs native pseudo-random number generator and
prepended to the ciphertext to be sent along. They must be further hashed in order
to know in advance whether they've been modified, or have some other means of
detecting modified messages.

*/
func (c CBCCrypt) Encrypt(plaintext string, key bitstring) (ciphertext bitstring, err error) {

	// generate an IV set (in CBC, IV is the same length as the block size)
	iv, _ := generateIV(AESBlockSize)

	// CBC mode works on blocks so plaintexts may need to be padded to the
	// next whole block.  By the time we get here, we assume the plaintext already
	// includes the MAC security value (if any)
	if padding := (len(plaintext) + 1) % AESBlockSize; padding != 0 {

		// how many bytes of padding to be the correct block size?
		padding = AESBlockSize - padding

		// fill the space with '=' similar to RSA plaintext x509 technique
		padval := make(bitstring, 0)
		for i := 0; i < padding; i++ {
			padval = append(padval, '=')
		}

		plaintext = plaintext + string(padval) + string(padding+1) // final byte is padding length
	} else {
		plaintext = plaintext + string(0) // the final byte is padding length, 0 in this case
	}

	// here we allocate a new aes block stream
	block, err := aes.NewCipher(key)
	if err != nil {
		log.Printf(err.Error())
		return bitstring(""), err
	}

	// here we allocate the memory for our ciphertext, and
	// prepend the IV values to it.  We don't need the IV to be
	// secret, just unique.
	ciphertext = make(bitstring, AESBlockSize+len(plaintext))
	copy(ciphertext[:aes.BlockSize], iv)

	// this data structure allows us to use the IV values from one block to
	// encrypt the next.
	stream := cipher.NewCBCEncrypter(block, iv)

	// the slice offset pushes the first block
	// the initial IV values
	stream.CryptBlocks(ciphertext[AESBlockSize:], bitstring(plaintext))

	// It's important to remember that ciphertexts must be authenticated
	// (i.e. by using crypto/hmac) as well as being encrypted in order to
	// be secure.

	return ciphertext, nil
}

/*
Wraps the native Golang Cipher Block Chain cryptographic algorithm.  From
the GoDocs :

""
CBC provides confidentiality by xoring (chaining) each plaintext block
with the previous ciphertext block before applying the block cipher.

See NIST SP 800-38A, pp 10-11
""

Decrypt takes the ciphertext byte stream produced by encrypt and a byte string
key that is length AESBlockSize == 16 and is the SAME key used to encrypt the
block, returns the decrypted text or an error if one occured.  There is no
verification of integrity beyond the ciphertext is the correct length!

*/
func (c CBCCrypt) Decrypt(ciphertext bitstring, key bitstring) (plaintext string, err error) {
	// do a sanity check on the ciphertext length
	//  must be [IV values] + [multiple of AESBlockSize]
	if (len(ciphertext)-AESBlockSize)%AESBlockSize != 0 {
		log.Printf("[Error] -> ciphertext is not correct length (did ciphertext MAC validate?)")
		return "", ErrInvalidArgs
	}

	// here we allocate a new aes block stream (not really a streaming algorithm,
	// but the method of chaining blocks resembles a stream in implementation)
	block, err := aes.NewCipher(key)
	if err != nil {
		log.Printf(err.Error())
		return "", err
	}

	// The IV is prepended to the ciphertext in our algorithm - it doesn't need to be
	// secure, only unique between any two instances of the same key.  We take advantage
	// of Go's slices to easily 'move' data around (it's really just pointers moving)
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	// SANITY CHECK: Make sure we're working with correctly sized blocks
	if len(ciphertext)%aes.BlockSize != 0 {
		log.Printf("[Error] -> ciphertext is not an even multiple of block size")
		return "", ErrInvalidArgs
	}

	mode := cipher.NewCBCDecrypter(block, iv)

	// CryptBlocks can operate on a destination that is the same as its source
	mode.CryptBlocks(ciphertext, ciphertext)

	// check for padding values
	if padding := ciphertext[len(ciphertext)-1:]; padding[0] != 0 {

		if int(padding[0]) >= AESBlockSize {
			//log.Printf("[Warning] -> padding value is corrupted. Data stream has been tampered with.")
			//return "", ErrInvalidArgs
		} else {
			// and remove padding if necessary
			plaintext = string(ciphertext[:len(ciphertext)-int(padding[0])])
		}
	} else {
		plaintext = string(ciphertext[:len(ciphertext)-1])
	}

	return plaintext, nil

}

func (c CBCCrypt) Describe() (howtouse string, err error) {
	return "", ErrNotImplemented
}

func (c CBCCrypt) Implements() (cops []string, err error) {
	ops := []string{
		"aes-cbc-192",
	}

	return ops, nil
}
