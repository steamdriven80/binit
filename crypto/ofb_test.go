package crypto

import (
	"encoding/hex"
	"testing"
)

func TestOFBCrypt(t *testing.T) {

	ofb := OFBCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	ciphertext, err := ofb.Encrypt(message, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	plaintext, err := ofb.Decrypt(ciphertext, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	if plaintext != message {
		t.Error("Decrypted Msg != Original Msg")
	}
}

func BenchmarkOFBEncrypt(b *testing.B) {
	ofb := OFBCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		ofb.Encrypt(message, []byte(key))
	}
}

func BenchmarkOFBDecrypt(b *testing.B) {
	ofb := OFBCrypt{}

	message, _ := hex.DecodeString("b92deed33475c43adf26600f6b4dd62a987c80e988b7ad189903f9137528d41bf4d8cb0c3e62")
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		ofb.Decrypt(message, []byte(key))
	}
}
