package crypto

import (
	"crypto/aes"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
)

// the AES block size (16 bytes)
const AESBlockSize = aes.BlockSize

// our internal error structures
var ErrNotImplemented = errors.New("binit/crypto -> method not implemented")
var ErrInternalFatal = errors.New("binit/crypto -> unexpected and unrecoverable fatal error")
var ErrInvalidArgs = errors.New("binit/crypto -> invalid arguments")

// we clarify the difference between a string (also a []byte) and a bitstring
// which more than likely doesn't contain printable characters
type bitstring []byte

// our algorithm containers
var cryptoAlg []interface{} = make([]interface{}, 0)
var hashAlg []interface{} = make([]interface{}, 0)

// the interfaces a crypto algorithm must fulfill to be useful to us
type CryptoSet interface {
	Encrypt(string, bitstring) (bitstring, error)
	Decrypt(bitstring, bitstring) (string, error)

	Describe() (string, error)
	Implements() ([]string, error)
}

/*
Convenience functions that allow for inclusion of Encrypt and Decrypt inline
in other functions (or assigned to const values)
*/
func MustEncrypt(value bitstring, err error) bitstring {
	if err != nil {
		panic(err)
	}

	return value
}

func MustDecrypt(value string, err error) string {
	if err != nil {
		panic(err)
	}

	return value
}

/*
Convenience function that converts a variable length password into a fixed N digit
encryption key, where N = 16 for all our AES Block variations
*/
func Password2Key(pw string) []byte {
	h := SHA256Hash{}

	key, _ := h.Hash(bitstring(pw), nil)

	return []byte(hex.EncodeToString(key)[16:32])
}

// the interfaces a hash algorithm must fulfill to be useful to us
type HashSet interface {
	Hash(msg bitstring, key bitstring) (hash bitstring, err error)
	Verify(hashA bitstring, hashB bitstring) (err error)

	Describe() (string, error)
	Implements() ([]string, error)
}

// the interface an algorithm must implement for us to describe/print info about it
type Algorithm interface {
	Describe() (string, error)
	Implements() ([]string, error)
}

// crypto setup function
func init() {
	//	fmt.Println("BinIT PasteBin Encryption Utility")
	//	fmt.Println("https://github.com/gearmover/binit")

	RegisterCrypto(CBCCrypt{})
	RegisterCrypto(CFBCrypt{})
	RegisterCrypto(CTRCrypt{})
	RegisterCrypto(OFBCrypt{})

	RegisterHash(HMACHash{})
	//	RegisterHash(SHA1Hash{})
	//	RegisterHash(SHA256Hash{})
}

// utility function to register a crypto algorithm
func RegisterCrypto(cryptFunc CryptoSet) {
	cryptoAlg = append(cryptoAlg, cryptFunc)
}

// utility function to register a hash algorithm
func RegisterHash(hashFunc HashSet) {
	hashAlg = append(hashAlg, hashFunc)
}

func PrintOps() {

	fmt.Println("Available encryption algorithms:\n")
	printValidOps(cryptoAlg)

	fmt.Println("\nAvailable hashing algorithms:\n")
	printValidOps(hashAlg)

	fmt.Println("")
}

func printValidOps(valid []interface{}) {

	ops := make([]string, 0)

	for _, crypt := range valid {
		tops, err := crypt.(Algorithm).Implements()
		if err != nil {
			log.Println(err)
			return
		}

		ops = append(ops, tops...)
	}

	const opsPerLine = 4
	const maxWidth = 80

	format := "%" + strconv.Itoa(int(maxWidth/opsPerLine)) + "s"

	for i, op := range ops {
		if i != 0 && i%opsPerLine == 0 {
			fmt.Printf("\n")
		}

		fmt.Printf(format, op)
	}

	fmt.Println()
}

// generates a random string of Initialization Values from Golang's cryptographic
// pseudo-random number generator.  Ensures the returned slice is byteSize bytes long.
func generateIV(byteSize int) (bitstring, error) {

	iv := make(bitstring, byteSize)

	_, err := io.ReadFull(rand.Reader, iv)
	if err != nil {
		return nil, err
	}

	return iv, nil
}
