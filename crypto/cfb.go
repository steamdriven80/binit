package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"log"
)

type CFBCrypt struct{}

func (c CFBCrypt) Encrypt(plaintext string, key bitstring) (ciphertext bitstring, err error) {

	// allocate ourselves a new cipher stream block of memory
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err) // it'd be an out of memory error error at this point, just die
	}

	// stream ciphers have no rigid size requirements like block ciphers. Woohoo! Such
	// a small function!

	// similar to the other algorithms, the IV only needs to be unique, rather than secret
	// here we prepend the IV to our final ciphertext
	ciphertext = make(bitstring, AESBlockSize+len(plaintext))
	iv, _ := generateIV(AESBlockSize)
	copy(ciphertext[:AESBlockSize], iv)

	// this is our actual streaming block of memory, that takes in a stream of plaintext, and a stream
	// of IVs (affine-style) and xors them together
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], []byte(plaintext))

	return ciphertext, nil
}

func (c CFBCrypt) Decrypt(ciphertext bitstring, key bitstring) (plaintext string, err error) {

	// allocate some scratch space to work
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err) // uh oh, memory error...
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < AESBlockSize {
		log.Printf("[Warning] -> the ciphertext stream provided is too short to be valid. (was stream validated?)")
		return "", ErrInvalidArgs
	}

	// we prepend the IVs to the ciphertext upon encryption, they don't have to
	// be kept secret after all (just unique!)
	iv := ciphertext[:AESBlockSize]
	ciphertext = ciphertext[AESBlockSize:]

	// our stream scratch space
	stream := cipher.NewCFBDecrypter(block, iv)

	// and our xor'ing method that can operate on the memory IN-PLACE,
	// i.e. the destination buffer is the same as the source buffer
	stream.XORKeyStream(ciphertext, ciphertext)

	return string(ciphertext), nil
}

func (c CFBCrypt) Describe() (howtouse string, err error) {
	return "", ErrNotImplemented
}

func (c CFBCrypt) Implements() (cops []string, err error) {
	ops := []string{
		"aes-cfb-192",
	}

	return ops, nil
}
