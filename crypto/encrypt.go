// encrypt
package crypto

import (
	"flag"
	//	"crypto/aes"
	//	"crypto/hmac"
	//	"crypto/sha256"

	//	"log"
	//	"math"
	//	"strconv"
)

var (
	EncSign        bool
	EncMaxBlobSize int
	EncSink        string
	EncSource      string
	EncCryptoAlg   string
	EncHmacAlg     string
)

func Run(Args []string) {

	flag.BoolVar(&EncSign, "s", true, "Sign the encrypted message with your SSH key")
	flag.IntVar(&EncMaxBlobSize, "b", 1024, "Maximum size (in bytes) of a single encrypted blob")
	flag.StringVar(&EncSource, "i", "-", "Source of input data to encrypt, '-' for stdin")
	flag.StringVar(&EncSink, "o", "-", "Sink for output encrypted data, '-' for stdout")
	flag.StringVar(&EncCryptoAlg, "c", "cbc128", "The cryptographic function to use, must be the same for both operations (encrypt and decrypt)")
	flag.StringVar(&EncHmacAlg, "h", "hmac", "The hashing function to use to ensure ciphertext integrity")

	flag.Parse()

}
