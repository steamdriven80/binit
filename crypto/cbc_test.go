package crypto

import (
	"encoding/hex"
	"testing"
)

func TestCBCCrypt(t *testing.T) {

	cbc := CBCCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	ciphertext, err := cbc.Encrypt(message, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	plaintext, err := cbc.Decrypt(ciphertext, []byte(key))
	if err != nil {
		t.Fatal(err)
	}

	if plaintext != message {
		t.Error("Decrypted Msg != Original Msg")
	}
}

func BenchmarkCBCEncrypt(b *testing.B) {
	cbc := CBCCrypt{}

	const message = "Hello Encrypted World!"
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		cbc.Encrypt(message, []byte(key))
	}
}

func BenchmarkCBCDecrypt(b *testing.B) {
	cbc := CBCCrypt{}

	message, _ := hex.DecodeString("d938612b08769ac7cfd034b8477a45c92abca54962c4fa23072724d89f3b25491a2b2b2f0ab76103d454b03584db93b0")
	const key = "My Super Secret "

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		cbc.Decrypt(message, []byte(key))
	}
}
